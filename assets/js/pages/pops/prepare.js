jQuery(document).ready(function () {
  $('#verify-date').datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true
  });
});
