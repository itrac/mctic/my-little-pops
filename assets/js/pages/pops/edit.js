jQuery(document).ready(function () {
  var wysiwygareaAvailable = isWysiwygareaAvailable(),
  isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

  var editorElement = CKEDITOR.document.getById('procedure-editor');

  if (wysiwygareaAvailable) {
    CKEDITOR.replace('procedure-editor');
  } else {
    editorElement.setAttribute('contenteditable', 'true');
    CKEDITOR.inline('procedure-editor');
  }
});

function isWysiwygareaAvailable() {
  if (CKEDITOR.revision == ('%RE' + 'V%')) {
    return true;
  }

  return !!CKEDITOR.plugins.get('wysiwygarea');
}
