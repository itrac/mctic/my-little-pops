jQuery(document).ready(function() {
  var exportColumns = [0, 1, 2];
  var table = $('#table').DataTable({
    buttons: [{
        extend: 'excel',
        exportOptions: {
          columns: exportColumns
        }
      },
      {
        extend: 'csv',
        exportOptions: {
          columns: exportColumns
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: exportColumns
        }
      },
      {
        extend: 'print',
        text: 'Imprimir',
        exportOptions: {
          columns: exportColumns
        }
      }
    ],
    columnDefs: [{
      targets: 3,
      orderable: false
    }],
    dom: "<'row'<'col-md text-center'B>>" +
      "<'row'<'col-md-7'l><'col-md-5'f>>" +
      "<'row'<'col-md'tr>>" +
      "<'row'<'col-md'i><'col-md'p>>",
    language: {
      url: '/modules/datatables.net/Portuguese-Brasil.json'
    },
    responsive: true,
    scrollCollapse: true,
    scrollY: '50vh'
  });
});