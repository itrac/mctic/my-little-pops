jQuery(document).ready(function () {
  var wysiwygareaAvailable = isWysiwygareaAvailable(),
  isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

  var editorElement = CKEDITOR.document.getById('template-content');

  if (isBBCodeBuiltIn) {
    editorElement.setHtml(
    );
  }

  if (wysiwygareaAvailable) {
    CKEDITOR.replace('template-content');
  } else {
    editorElement.setAttribute('contenteditable', 'true');
    CKEDITOR.inline('template-content');
  }
});

function isWysiwygareaAvailable() {
  if (CKEDITOR.revision == ('%RE' + 'V%')) {
    return true;
  }

  return !!CKEDITOR.plugins.get('wysiwygarea');
}
