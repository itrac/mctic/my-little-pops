CKEDITOR.editorConfig = function(config) {
  config.language = 'pt-BR';
  config.extraPlugins =
  'a11yhelp,about,adobeair,ajax,autoembed,autogrow,autolink,balloonpanel,bidi,codesnippet,codesnippetgeshi,colorbutton,colordialog,copyformatting,dialog,dialogadvtab,div,divarea,docprops,embedbase,embedsemantic,filetools,find,flash,font,forms,iframe,iframedialog,image,image2,indentblock,justify,language,lineutils,link,liststyle,magicline,newpage,notificationaggregator,pagebreak,panelbutton,pastefromword,placeholder,preview,print,save,scayt,selectall,sharedspace,showblocks,smiley,sourcedialog,specialchar,stylesheetparser,table,tableresize,tableselection,tabletools,templates,uicolor,uploadwidget,widget,widgetselection,wsc,xml';
};
