module.exports = function(grunt) {
  grunt.registerTask('compileAssets', [
    'clean:dev',
    'jst:dev',
    'sass:dev',
    'copy:dev',
    'copy:dev_modules',
    'copy:dev_modifiers',
    'coffee:dev'
  ]);
};
