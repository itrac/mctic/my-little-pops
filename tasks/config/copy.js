module.exports = function(grunt) {

  grunt.config.set('copy', {
    dev: {
      files: [{
        expand: true,
        cwd: './assets',
        src: ['**/*.!(coffee|less|scss|sass)'],
        dest: '.tmp/public'
      }]
    },
    dev_modules: {
      files: [
        {
          expand: true,
          cwd: './node_modules/bootstrap/dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/bootstrap'
        },
        {
          expand: true,
          cwd: './node_modules/bootstrap-tagsinput/dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/bootstrap-tagsinput'
        },
        {
          expand: true,
          cwd: './node_modules/ckeditor',
          src: ['**/*', '!config.js'],
          dest: '.tmp/public/modules/ckeditor'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net/js',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net-bs4',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net/bs4'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net-buttons',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net/buttons'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net-buttons-bs4',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net/buttons/bs4'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net-responsive',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net/responsive'
        },
        {
          expand: true,
          cwd: './node_modules/datatables.net-responsive-bs4',
          src: ['**/*'],
          dest: '.tmp/public/modules/datatables.net/responsive/bs4'
        },
        {
          expand: true,
          cwd: './node_modules/font-awesome',
          src: ['**/*'],
          dest: '.tmp/public/modules/font-awesome'
        },
        {
          expand: true,
          cwd: './node_modules/jquery/dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/jquery'
        },
        {
          expand: true,
          cwd: './node_modules/jquery-ui-dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/jquery-ui'
        },
        {
          expand: true,
          cwd: './node_modules/jszip/dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/jszip'
        },
        {
          expand: true,
          cwd: './node_modules/mdi',
          src: ['**/*'],
          dest: '.tmp/public/modules/mdi'
        },
        {
          expand: true,
          cwd: './node_modules/pdfmake/build',
          src: ['**/*'],
          dest: '.tmp/public/modules/pdfmake'
        },
        {
          expand: true,
          cwd: './node_modules/popper.js/dist',
          src: ['**/*'],
          dest: '.tmp/public/modules/popper.js'
        }
      ]
    },
    dev_modifiers: {
      files: [{
        expand: true,
        cwd: './assets/modules/ckeditor',
        src: ['config.js'],
        dest: '.tmp/public/modules/ckeditor'
      }],
      files: [{
        expand: true,
        cwd: './assets/modules/datatables.net',
        src: ['Portuguese-Brasil.json'],
        dest: '.tmp/public/modules/datatables.net'
      }]
    },
    build: {
      files: [{
        expand: true,
        cwd: '.tmp/public',
        src: ['**/*'],
        dest: 'www'
      }]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
};
