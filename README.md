# My Little POPs

**My Little POPs** (**MLP**) é um sistema _web_ que tem por objetivo apoiar as tarefas de realocação de **Procedimentos Operacionais** (**POPs**) em `.pdf`/`.docx` para o sistema [CITSmart](http://www.citsmart.com/) do [MCTIC](http://www.mctic.gov.br/). Tais tarefas são destinadas para a equipe do ITRAC responsável pelo **Projeto Aprimoramento do _Framework_ de Soluções de Tecnologia da Informação para o Ministério das Comunicações** (GEPRO TED MC _Framework_ 2015).

## Características do Sistema

* **Nome**: My Little POPs
* **Acrônimo**: MLP
* **Linguagens**: JavaScript, HTML, CSS
* _**Framework**_: [Sails.js](http://sailsjs.com/)
* **Banco de Dados**: NoSQL ([MongoDB](https://www.mongodb.com/))
* **Ambiente**:
  * _Container_ [Docker](https://www.docker.com/) com imagem [node](https://hub.docker.com/_/node/), de nome `mlp`
  * _Container_ [Docker](https://www.docker.com/) com imagem [mongo](https://hub.docker.com/_/mongo/), de nome `mlp-mongo`

## Instalação Local

* **Pré-Requisitos**:
  1. [Docker](https://www.docker.com/) instalado;
  1. [Docker Compose](https://docs.docker.com/compose/) instalado;
  1. Porta `80` disponível para uso.
* **Passo 1**: Execute o script `sbin/docker/up.sh` na raíz do projeto;
* **Passo 2**: Acesse `localhost/`

## Time de Desenvolvimento

|Nome|GitLab|Github|
|:-:|:-:|:-:|
|Allan Jefrey|[@AllanNobre](https://gitlab.com/AllanNobre)|[@AllanNobre](https://github.com/AllanNobre)|
|Jonathan Moraes|[@arkye](https://gitlab.com/arkye)|[@arkye](https://github.com/arkye)|
|Leonardo Oliveira|[@leonardork](https://gitlab.com/leonardork)|[@LeonardoRk](https://github.com/LeonardoRk)|
|Tayná|[@taaynaandrade](https://gitlab.com/taaynaandrade)|[@taaynaandrade](https://github.com/taaynaandrade)|
