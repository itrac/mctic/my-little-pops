module.exports.connections = {
  mongo: {
    adapter: 'sails-mongo',
    host: 'db',
    port: 27017,
    database: 'mlp'
  }
};
