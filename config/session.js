module.exports.session = {
  adapter: 'connect-mongo',
  url: 'mongodb://db/mlp',
  collection: 'sessions',
  auto_reconnect: false,
  secret: 'fa5a6caf19c86623fbe57f8dc7b57bbf'
};
