module.exports.routes = {
  '/': {
    view: 'home',
    policy: 'isAuthenticated'
  },

  '/api/ready_pops': 'ApiController.readyPops',
  '/api/five_pops': 'ApiController.fivePops',
  '/api/paged_pops': 'ApiController.pagedPops',

  'get /login': {
    view: 'users/login'
  },
  'post /login': 'UserController.login',
  'get /signup': {
    view: 'users/signup'
  },
  'post /signup': 'UserController.signup',
  '/logout': 'UserController.logout',

  '/pops/new': 'PopController.new',
  'post /pops/parse': 'PopController.parse',
  'get /pops/show/:id': 'PopController.show',
  'get /pops/edit/:id': 'PopController.edit',
  'post /pops/edit': 'PopController.edit',
  '/pops/prepare_for_verification': 'PopController.prepareForVerification',
  '/pops/list': 'PopController.list',
  '/pops/list/unverified': 'PopController.listUnverified',
  'post /pops/verify': 'PopController.verify',
  '/pops/list/verified': 'PopController.listVerified',
  '/pops/list/to_delete': 'PopController.listToDelete',
  '/pops/list/with_pendencies': 'PopController.listWithPendencies',
  '/pops/list/without_pendencies': 'PopController.listWithoutPendencies',
  '/pops/list/disposables': 'PopController.listDisposables',
  '/pops/list/updated': 'PopController.listUpdated',
  '/pops/list/ready': 'PopController.listReady',

  '/summary': 'SummaryController.show',

  '/templates/new': 'TemplateController.new',
  '/templates/show/:id': 'TemplateController.show',
  'get /templates/edit/:id': 'TemplateController.edit',
  'post /templates/edit': 'TemplateController.edit',
  '/templates/list': 'TemplateController.list',
  '/templates/set_default/:id': 'TemplateController.updateDefault',

  '/headers/new': 'HeaderController.new',
  'get /headers/edit/:id': 'HeaderController.edit',
  'post /headers/edit': 'HeaderController.edit',
  '/headers/list': 'HeaderController.list',
  '/headers/set_default/:id': 'HeaderController.setDefault'
};
