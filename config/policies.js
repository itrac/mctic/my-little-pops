module.exports.policies = {
  '*': 'isAuthenticated',

  UserController: {
    '*': 'isAuthenticated',
    login: true,
    signup: true
  },

  PopsController: {
    '*': 'isAuthenticated'
  },

  ApiController: {
    '*': true
  }
};
