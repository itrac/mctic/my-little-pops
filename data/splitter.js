var source = require('./source.json');
var body = require('./source.json');
var fs = require('fs');

var pops = source.pops;
let encode = 'utf-8';
let callback = (error) => {
  if(error) {
    throw error;
  } else {
    console.log('Success!');
  }
}
pops.forEach((pagedPops, index) => {
  let filename = index + '.json';
  body.length = pagedPops.length;
  body.pops = pagedPops;
  fs.writeFile(filename, JSON.stringify(body), encode, callback);
});
