FROM node:boron

WORKDIR /code
ADD package.json /code/package.json
RUN npm install pm2@latest -g
RUN npm install sails@latest -g
RUN npm install
ADD . /code
