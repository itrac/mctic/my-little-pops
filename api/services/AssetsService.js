module.exports = {
  bootstrapTagsinput: {
    styles: [
      'modules/bootstrap-tagsinput/bootstrap-tagsinput'
    ],
    scripts: [
      'modules/bootstrap-tagsinput/bootstrap-tagsinput.min'
    ]
  },
  ckeditor: {
    scripts: [
      'modules/ckeditor/ckeditor'
    ]
  },
  datatables: {
    styles: [
      'modules/datatables.net/bs4/css/dataTables.bootstrap4',
      'modules/datatables.net/buttons/bs4/css/buttons.bootstrap4.min',
      'modules/datatables.net/responsive/bs4/css/responsive.bootstrap4.min'
    ],
    scripts: [
      'modules/jszip/jszip.min',
      'modules/pdfmake/pdfmake.min',
      'modules/pdfmake/vfs_fonts',
      'modules/datatables.net/jquery.dataTables',
      'modules/datatables.net/buttons/js/dataTables.buttons.min',
      'modules/datatables.net/buttons/js/buttons.html5.min',
      'modules/datatables.net/buttons/js/buttons.flash.min',
      'modules/datatables.net/buttons/js/buttons.print.min',
      'modules/datatables.net/responsive/js/dataTables.responsive.min',
      'modules/datatables.net/bs4/js/dataTables.bootstrap4',
      'modules/datatables.net/buttons/bs4/js/buttons.bootstrap4.min',
      'modules/datatables.net/responsive/bs4/js/responsive.bootstrap4.min'
    ]
  },
  jqueryUI: {
    styles: [
      'modules/jquery-ui/jquery-ui.min'
    ],
    scripts: [
      'modules/jquery-ui/jquery-ui.min'
    ]
  }
};
