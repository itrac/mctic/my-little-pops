module.exports = {
  new: require('./headers/new.js'),
  edit: require('./headers/edit.js'),
  list: require('./headers/list.js')
};
