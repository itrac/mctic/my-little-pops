module.exports = {
  update: function(req, res) {
    var id = req.param('id');

    var callback = function(err, header) {
      if (!err) {
        addNewFieldAndUpdate(req, res, id, header.fields);
      } else {
        var message = 'Erro ao acessar cabeçalho no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/headers/edit/' + id);
      }
    };

    Header.get(id, callback);
  },
  show: function(req, res) {
    var id = req.param('id');
    var callback = function(err, header) {
      if (!err) {
        var endpoint = 'headers/edit';

        if (header) {
          var data = {
            page: '/' + endpoint,
            header: header
          };
          return res.view(endpoint, data);
        } else {
          var data = {
            page: '/' + endpoint
          };
          return res.view(endpoint, data);
        }
      } else {
        var message = 'Erro ao tentar acesar um cabeçalho no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Header.get(id, callback);
  }
};

function addNewFieldAndUpdate(req, res, id, fields) {

  for (var key in fields) {
    var fieldName = req.param(key + 'Name');
    if (fieldName == 'DELETE') {
      delete fields[key];
    } else {
      fields[key] = fieldName;
    }
  }

  if (req.param('fieldKey') != '' && req.param('fieldName') != '') {
    fields[req.param('fieldKey')] = req.param('fieldName');
  }

  var data = {
    name: req.param('name'),
    fields: fields
  };

  var callback = function(err, header) {
    if (!err) {
      var message = 'Atualização de cabeçalho bem sucedida!';
      AlertService.success(req, message);
    } else {
      var message = 'Erro ao atualizar cabeçalho no banco de dados!';
      AlertService.error(req, err, message);
    }
    return res.redirect('/headers/edit/' + id);
  }

  Header.updateOne(id, data, callback);
};
