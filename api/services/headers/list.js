module.exports = {
  list: function(req, res) {
    var callback = function(err, headers) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/headers/list');

        var endpoint = 'headers/list';
        var data = {
          page: '/' + endpoint,
          headers: headers
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por cabeçalhos!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    }

    Header.getAll(callback);
  },
  setDefault: function(req, res) {
    var callback = function(err, header) {
      if (!err) {
        setToDefault(req, res);
      } else {
        var message = 'Erro ao atualizar antigo cabeçalho padrão!';
        AlertService.error(req, err, message);
        return res.redirect('/headers/list');
      }
    }

    Header.setAllDefaultToFalse(callback);
  }
};

function setToDefault(req, res) {
  var callback = function(err, lastHeader) {
    if (!err) {
      var message = 'Cabeçalho padrão atualizado com sucesso!';
      AlertService.success(req, message);
    } else {
      var message = 'Erro ao atualizar cabeçalho para padrão!';
      AlertService.error(req, err, message);
    }
    return res.redirect('/headers/list');
  }

  Header.setDefault(req.param('id'), callback);
}
