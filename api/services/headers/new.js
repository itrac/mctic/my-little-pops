module.exports = {
  create: function(req, res) {
    var callback = function(err, quantity) {
      if (!err) {
        var isDefault = quantity == 0;
        createHeader(req, res, isDefault);
      } else {
        var message = 'Erro ao realizar contagem de cabeçalhos no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/headers/list');
      }
    }

    Header.getQuantity(callback);
  },
  show: function(req, res) {
    var endpoint = 'headers/new';
    var data = {
      page: '/' + endpoint
    };
    return res.view(endpoint, data);
  }
};

function createHeader(req, res, isDefault) {
  var header = {
    name: req.param('name'),
    author: req.session.name,
    isDefault: isDefault
  };

  var callback = function(err, header) {
    if (!err) {
      var message = 'Criação de cabeçalho bem sucedida!';
      req.session.hasHeader = true;
      AlertService.success(req, message);
      return res.redirect('/headers/list');
    } else {
      var message = 'Erro ao inserir cabeçalho no banco de dados!';
      AlertService.error(req, err, message);
      return res.redirect('/headers/new');
    }
  }

  Header.createOne(header, callback);
}
