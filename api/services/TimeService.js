module.exports = {
  toBrazillianDate: function (date) {
    var day = date.getDate() + 1;
    return day + "/" + date.getMonth() + "/" + date.getFullYear();
  },
  toDatabaseDate: function (date) {
    var fractions = date.split("/");
    return fractions[1] + "/" + fractions[0] + "/" + fractions[2];
  }
};
