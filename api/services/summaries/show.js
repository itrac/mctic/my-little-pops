module.exports = {
  quantityData: function(req, res) {
    getAllPOPsAndContinue(req, res);
  }
};

function getAllPOPsAndContinue(req, res) {
  var callback = function(err, pops) {
    if (!err) {
      var summary = buildSummary(pops);
      showQuantityDataView(res, req, summary);
    } else {
      console.log(err);
      req.addFlash('error', 'Erro ao acessar informações de POPs!');
      return res.redirect('/');
    }
  };
  Pop.getAll(callback);
};

function buildSummary(pops) {
  var insertedSheets = {};
  var insertedLevels = {};
  var summary = {
    levels: [],
    sheets: [],
    quantity: {
      total: 0
    },
    toDelete: {
      total: 0
    },
    withPendencies: {
      total: 0
    },
    withoutPendencies: {
      total: 0
    },
    updated: {
      total: 0
    },
    ready: {
      total: 0
    }
  };
  var updateSummary = function(pop) {
    var id = pop.level.id;
    if (insertedLevels[id] == undefined) {
      insertedLevels[id] = true;
      summary.levels.push(pop.level);
      summary.quantity[id] = {
        total: 0,
        sheets: {}
      };
      summary.toDelete[id] = {
        total: 0,
        sheets: {}
      };
      summary.withPendencies[id] = {
        total: 0,
        sheets: {}
      };
      summary.withoutPendencies[id] = {
        total: 0,
        sheets: {}
      };
      summary.updated[id] = {
        total: 0,
        sheets: {}
      };
      summary.ready[id] = {
        total: 0,
        sheets: {}
      };
    }
    if (insertedSheets[pop.referenceSheet] == undefined) {
      insertedSheets[pop.referenceSheet] = true;
      summary.sheets.push(pop.referenceSheet);
      summary.quantity[id].sheets[pop.referenceSheet] = 0;
      summary.toDelete[id].sheets[pop.referenceSheet] = 0;
      summary.withPendencies[id].sheets[pop.referenceSheet] = 0;
      summary.withoutPendencies[id].sheets[pop.referenceSheet] = 0;
      summary.updated[id].sheets[pop.referenceSheet] = 0;
      summary.ready[id].sheets[pop.referenceSheet] = 0;
    }
    summary.quantity.total++;
    summary.quantity[id].total++;
    summary.quantity[id].sheets[pop.referenceSheet]++;
    if(pop.state == 'Para excluir') {
      summary.toDelete.total++;
      summary.toDelete[id].total++;
      summary.toDelete[id].sheets[pop.referenceSheet]++;
    } else if(pop.state == 'Com pendências') {
      summary.withPendencies.total++;
      summary.withPendencies[id].total++;
      summary.withPendencies[id].sheets[pop.referenceSheet]++;
    } else if(pop.state == 'Sem pendências') {
      summary.withoutPendencies.total++;
      summary.withoutPendencies[id].total++;
      summary.withoutPendencies[id].sheets[pop.referenceSheet]++;
    } else if(pop.state == 'Atualizado') {
      summary.updated.total++;
      summary.updated[id].total++;
      summary.updated[id].sheets[pop.referenceSheet]++;
    } else if(pop.state == 'Pronto') {
      summary.ready.total++;
      summary.ready[id].total++;
      summary.ready[id].sheets[pop.referenceSheet]++;
    }
  };
  for(var index  = 0; index < pops.length; index++) {
    updateSummary(pops[index]);
  }
  return summary;
};

function showQuantityDataView(res, req, summary) {
  var endpoint = 'summaries/show';
  var data = {
    page: '/' + endpoint,
    summary: summary
  };
  return res.view(endpoint, data);
};
