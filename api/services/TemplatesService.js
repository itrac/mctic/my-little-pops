module.exports = {
  new: require('./templates/new.js'),
  show: require('./templates/show.js'),
  edit: require('./templates/edit.js'),
  list: require('./templates/list.js')
};
