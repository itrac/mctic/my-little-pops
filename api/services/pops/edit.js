module.exports = {
  update: function(req, res) {
    var id = req.param('id');

    var callback = function(err, pop) {
      if (!err) {
        getHeaderAndUpdate(req, res, pop);
      } else {
        var message = 'Erro ao atualizar POP!';
        AlertService.error(req, err, message);
        return res.redirect('/pops/edit/' + id);
      }
    }

    Pop.get(id, callback);
  },
  updateWithData: function(req, res, data, callback) {
    var id = data.id;
    Pop.updateOne(id, data, callback);
  },
  show: function(req, res) {
    var id = req.param('id');

    var callback = function(err, pop) {
      if (!err) {
        pop.verifyDate = TimeService.toBrazillianDate(pop.verifyDate);
        getHeaderAndShow(req, res, pop);
      } else {
        var message = 'Erro ao tentar encontrar POP!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    }

    Pop.get(id, callback);
  }
};

function getData(req) {

  var data = {};

  if (req.param('number')) data.number = req.param('number');
  if (req.param('name')) data.name = req.param('name');
  if (req.param('observations')) data.observations = req.param('observations');
  if (req.param('actions')) data.actions = req.param('actions');
  if (req.param('accountablesFromITRAC')) data.accountablesFromITRAC = req.param('accountablesFromITRAC');
  if (req.param('accountablesFromMCTIC')) data.accountablesFromMCTIC = req.param('accountablesFromMCTIC');
  if (req.param('verifyDate')) TimeService.toDatabaseDate(req.param('verifyDate'));
  if (req.param('level')) data.level = req.param('level');
  if (req.param('procedure')) data.procedure = req.param('procedure');
  if (req.param('state')) data.state = req.param('state');
  if (req.param('category')) data.category = req.param('category');
  if (req.param('justification')) data.justification = req.param('justification');
  if (req.param('referenceSheet')) data.referenceSheet = req.param('referenceSheet');

  return data;
}

function getHeaderAndShow(req, res, pop) {
  var callback = function(err, header) {
    if (!err) {
      res.locals.scripts = AssetsService.datatables.scripts
        .concat(AssetsService.ckeditor.scripts)
        .concat('js/pages/pops/edit');

      var endpoint = 'pops/edit';
      var states = [
        'Não verificado',
        'Verificado',
        'Para excluir',
        'Com pendências',
        'Sem pendências',
        'Descartável',
        'Atualizado',
        'Pronto'
      ];
      var categories = [
        'Categoria não definida',
        'Antivírus',
        'Aplicativos de IRPF',
        'Aplicativos de PDF',
        'Certificado Digital',
        'Cliente Java',
        'Compactadores',
        'Comunicadores',
        'Dispositivos Móveis',
        'E-mail',
        'Estação de trabalho',
        'Impressora',
        'Internet e Wi-fi',
        'Login e permissão',
        'Módulos de Segurança Bancário',
        'Navegadores Web',
        'Pacote Office',
        'Pastas de Rede',
        'Player de Aúdio e Vídeo',
        'Software e Aplicativos',
        'Telefonia',
        'Videoconferência',
        'Windows'
      ];
      var data = {
        page: '/' + endpoint,
        header: header,
        states: states,
        categories: categories,
        pop: pop
      }

      return res.view(endpoint, data);
    } else {
      var message = 'Erro ao encontrar cabeçalho para a POP!';
      AlertService.error(req, err, message);
      return res.redirect('/');
    }
  }

  Header.getDefault(callback);
}

function getHeaderAndUpdate(req, res, pop) {
  var callback = function(err, header) {
    if (!err) {
      var data = getData(req);

      if (pop.fields) {
        data.fields = pop.fields;
      } else {
        data.fields = {};
      }

      for (var key in header.fields) {
        data.fields[key] = req.param(key);
      }

      updatePop(req, res, pop.id, data);
    } else {
      var message = 'Erro ao encontrar cabeçalho para a POP!';
      AlertService.error(req, err, message);
      return res.redirect('/pops/edit/' + pop.id);
    }
  }

  Header.getDefault(callback);
}

function updatePop(req, res, id, data) {
  var callback = function(err, pop) {
    if (!err) {
      var message = 'POP atualizada!';
      AlertService.success(req, message);
    } else {
      var message = 'Erro ao atualizar POP!';
      AlertService.error(req, err, message);
    }
    return res.redirect('/pops/show/' + id);
  }

  Pop.updateOne(id, data, callback);
}
