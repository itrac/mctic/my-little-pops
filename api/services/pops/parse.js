module.exports = {
  importFromCSV: function(req, res) {
    var criteria = {
      name: req.param('level')
    };

    var callback = function(err, level) {
      if (!err) {
        getFilesAndContinue(req, res, level);
      } else {
        var message = 'Erro ao inserir ou receber Nível de POP no banco de dados!';
        AlertService.error(req, err, message);
        redirect(res);
      }
    };

    PopLevel.get(criteria, callback);
  }
}

function redirect(res) {
  return res.redirect('/pops/new');
};

function getFilesAndContinue(req, res, level) {
  var options = {
    maxBytes: 1000000000
  };

  var callback = function(err, files) {
    if (!err) {
      readFileAndContinue(req, res, level, files);
    } else {
      var message = 'Erro ao tentar receber arquivo!';
      AlertService.error(req, err, message);
      redirect(res);
    }
  };

  req.file('file').upload(options, callback);
}

function readFileAndContinue(req, res, level, files) {
  var callback = function(err, data) {
    if (!err) {
      var referenceSheet = files[0].filename;
      parseAndFinish(req, res, level, data, referenceSheet);
    } else {
      var message = 'Erro ao tentar abrir arquivo!';
      AlertService.error(req, err, message);
      redirect(res);
    }
  }

  var fs = require('fs');
  fs.readFile(files[0].fd, 'utf-8', callback);
}

function parseAndFinish(req, res, level, data, referenceSheet) {
  var pops = extractPops(req, res, level, data, referenceSheet);
  if (pops) {
    var callback = function(err, pops) {
      if (!err) {
        var message = 'Importação de POPs bem sucedida!';
        AlertService.success(req, message);
      } else {
        var message = 'Erro ao inserir POPs importadas no banco de dados!';
        AlertService.error(req, err, message);
      }
      redirect(res);
    }

    Pop.createMany(pops, callback);
  }
}

function extractPops(req, res, level, data, referenceSheet) {
  var pops = [];
  try {
    var lines = data.split('\n');
    for (var line = 1; line < lines.length; line++) {
      var lineInfo = lines[line];
      if (lineInfo) {
        var info = lineInfo.split('\t');
        var pop = {};
        pop.level = level;
        pop.referenceSheet = referenceSheet;
        // Column A: Pop File
        var column = 0;
        var popFileNameItems = info[column].split(' - ');
        var popIdentifier = popFileNameItems[column].split(' ');
        pop.number = popIdentifier[1];
        pop.name = popFileNameItems[1].split('.')[0];
        // Column B: WasVerified?
        column++;
        pop.state = info[column] == 'S' ? 'Verificado' : 'Não verificado';
        // Column C: ToDelete?
        column++;
        if (pop.state == 'Verificado' && info[column] == 'S') {
          pop.state = 'Para excluir';
        }
        // Column D: Observations
        column++;
        pop.observations = info[column];
        if (info.length == column + 1) {
          column = 0;
          do {
            line++;
            lineInfo = lines[line];
            info = lineInfo.split('\t');
            pop.observations += '\t' + info[column];
          } while (info.length == 1);
        }
        // Column E: Actions
        column++;
        pop.actions = info[column];
        if (info.length == column + 1) {
          column = 0;
          do {
            line++;
            lineInfo = lines[line];
            info = lineInfo.split('\t');
            pop.actions += '\t' + info[column];
          } while (info.length == 1);
        }
        if (pop.state == 'Verificado') {
          if (pop.observations.length < 3 && pop.actions.length < 3) {
            pop.state = 'Sem pendências';
          } else {
            pop.state = 'Com pendências';
          }
        }
        // Column F: AccountablesFromITRAC
        column++;
        pop.accountablesFromITRAC = info[column];
        // Column G: AccountablesFromMCTIC
        column++;
        pop.accountablesFromMCTIC = info[column];
        // Column H: VerifyDate
        column++;
        pop.verifyDate = info[column];

        pops.push(pop);
      }
    }
  } catch (parseError) {
    var message = 'Erro ao tentar extrair dados do arquivo!'
    AlertService.error(req, err, message);
    redirect(res);
  }
  return pops;
}
