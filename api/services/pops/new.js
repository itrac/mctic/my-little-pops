module.exports = {
  createFromForm: function(req, res) {
    var criteria = {
      name: req.param('level')
    };

    var callback = function(err, level) {
      if (!err) {
        createPop(req, res, level);
      } else {
        var message = 'Erro ao inserir ou receber Nível de POP no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/pops/new');
      }
    };

    PopLevel.get(criteria, callback);
  },
  show: function(req, res) {
    var callback = function(err, levels) {
      if (!err) {
        var endpoint = 'pops/new';
        var data = {
          page: '/' + endpoint,
          popLevels: levels
        }
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao apresentar página! Caso você seja o administrador, consulte o log do servidor.';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    }

    PopLevel.getAll(callback);
  }
}

function createPop(req, res, level) {
  var pop = {
    number: req.param('number'),
    name: req.param('name'),
    referenceSheet: req.param('referenceSheet'),
    level: level
  };

  var callback = function(err, pop) {
    if (!err) {
      var message = 'Criação de POP bem sucedida!';
      AlertService.success(req, message);
    } else {
      var message = 'Erro ao inserir ou receber Nível de POP no banco de dados!';
      AlertService.error(req, err, message);
    }
    return res.redirect('/pops/new');
  }

  Pop.createOne(pop, callback);
}
