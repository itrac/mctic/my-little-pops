module.exports = {
  readyPops: function(req, res) {
    var callback = function(err, template) {
      if (!err) {
        template.content = template.content.replace(/\t/g, '');
        template.content = template.content.replace(/\n/g, '');
        template.content = template.content.replace(/\r/g, '');
        template.content = template.content.replace(/\"/g, '');
        getHeader(req, res, template, 'readyPops');
      } else {
        return res.json(500, err);
      }
    }

    Template.getDefault(callback);
  },
  fivePops: function(req, res) {
    var callback = function(err, template) {
      if (!err) {
        template.content = template.content.replace(/\t/g, '');
        template.content = template.content.replace(/\n/g, '');
        template.content = template.content.replace(/\r/g, '');
        template.content = template.content.replace(/\"/g, '');
        getHeader(req, res, template, 'fivePops');
      } else {
        return res.json(500, err);
      }
    }

    Template.getDefault(callback);
  },
  pagedPops: function(req, res) {
    var callback = function(err, template) {
      if (!err) {
        template.content = template.content.replace(/\t/g, '');
        template.content = template.content.replace(/\n/g, '');
        template.content = template.content.replace(/\r/g, '');
        template.content = template.content.replace(/\"/g, '');
        getHeader(req, res, template, 'pagedPops');
      } else {
        return res.json(500, err);
      }
    }

    Template.getDefault(callback);
  }
};

function getReadyPopsAndSend(req, res, template, header) {
  var callback = function(err, pops) {
    if (!err) {
      pops.forEach(function(pop, index) {
        if (pop.fields.preRequirements == undefined || pop.fields.preRequirements.length < 5) {
          pop.fields.preRequirements = "Não se aplica";
        }
        pop.procedure = pop.procedure.replace(/\t/g, '');
        pop.procedure = pop.procedure.replace(/\n/g, '');
        pop.procedure = pop.procedure.replace(/\r/g, '');
        pop.procedure = pop.procedure.replace(/\"/g, '');
      });
      var data = {
        length: pops.length,
        header: header,
        template: template,
        status: 'ready',
        pops: pops
      };
      return res.json(200, data);
    } else {
      return res.json(500, err);
    }
  };

  Pop.getAllReady(callback);
}

function getFivePopsAndSend(req, res, template, header) {
  var callback = function(err, pops) {
    if (!err) {
      if (pops.length > 5) {
        pops = pops.slice(0, 5);
      }
      pops.forEach(function(pop, index) {
        if (pop.fields.preRequirements == undefined || pop.fields.preRequirements.length < 5) {
          pop.fields.preRequirements = "Não se aplica";
        }
        pop.procedure = pop.procedure.replace(/\t/g, '');
        pop.procedure = pop.procedure.replace(/\n/g, '');
        pop.procedure = pop.procedure.replace(/\r/g, '');
        pop.procedure = pop.procedure.replace(/\"/g, '');
      });
      var data = {
        length: pops.length,
        header: header,
        template: template,
        status: 'ready',
        pops: pops
      };
      return res.json(200, data);
    } else {
      return res.json(500, err);
    }
  }

  Pop.getAllReady(callback);
}

function getPagedPopsAndSend(req, res, template, header) {
  var callback = function(err, pops) {
    if (!err) {
      var pagedPops = [];
      pops.forEach(function(pop, index) {
        if (pop.fields.preRequirements == undefined || pop.fields.preRequirements.length < 5) {
          pop.fields.preRequirements = "Não se aplica";
        }
        pop.procedure = pop.procedure.replace(/\t/g, '');
        pop.procedure = pop.procedure.replace(/\n/g, '');
        pop.procedure = pop.procedure.replace(/\r/g, '');
        pop.procedure = pop.procedure.replace(/\"/g, '');
      });
      var i, j;
      for(i = 0, j = 1; i < pops.length/50; i++, j++) {
        var initial = i*50;
        var final = j*50;
        if(final > pops.length) {
          final = pops.length;
        }
        pagedPops.push(pops.slice(initial, final));
      }
      var data = {
        length: pops.length,
        header: header,
        template: template,
        status: 'ready',
        pops: pagedPops
      };
      return res.json(200, data);
    } else {
      return res.json(500, err);
    }
  };

  Pop.getAllReady(callback);
}

function getHeader(req, res, template, context) {
  var callback = function(err, header) {
    if (!err) {
      if (context == 'readyPops') {
        getReadyPopsAndSend(req, res, template, header);
      } else if (context == 'fivePops') {
        getFivePopsAndSend(req, res, template, header);
      } else {
        getPagedPopsAndSend(req, res, template, header)
      }
    } else {
      return res.json(500, err);
    }
  }

  Header.getDefault(callback);
}
