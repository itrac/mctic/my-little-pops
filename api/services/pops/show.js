module.exports = {
  show: function(req, res) {
    var popId = req.param('id');
    var pop = Pop.get(popId, function(err, pop) {
      if (!err) {
        pop.verifyDate = TimeService.toBrazillianDate(pop.verifyDate);
        renderDefaultTemplate(req, res, pop);
      } else {
        console.log("erro ao pegar a POP no banco de dados");
      }
    });
  }
};

function renderDefaultTemplate(req, res, pop) {
  var callback = function(err, template) {
    if (!err) {
      var procedure = pop.procedure;
      var content = template.content;
      procedure = content.replace('[[PROCEDURE]]', pop.procedure);
      renderHeaderInTemplate(req, res, pop, procedure);
    } else {
      var message = 'Não foi possível pegar os templates do banco de dados';
      AlertService.error(req, err, message);
      return res.redirect('/');
    }
  }

  Template.getDefault(callback);
};

function renderHeaderInTemplate(req, res, pop, procedure) {
  var callback = function(err, header) {
    if (!err) {
      for (var key in header.fields) {
        if (pop.fields && pop.fields[key] != undefined) {
          procedure = procedure.replace('[[' + key + ']]', pop.fields[key]);
        } else {
          procedure = procedure.replace('[[' + key + ']]', '');
        }
        procedure = procedure.replace('{{' + key + '}}', header.fields[key]);
      }
      var endpoint = 'pops/show';
      return res.view(endpoint, {
        procedure: procedure,
        pop: pop,
        page: '/' + endpoint
      });
    } else {
      var message = 'Erro ao acessar cabeçalhos para a POP!';
      AlertService.error(req, err, message);
      return res.redirect('/');
    }
  }

  Header.getDefault(callback);
}
