module.exports = {
  save: function(req, res) {
    req.session.lastTimeReady = new Date().toDateString();
    req.session.verifyDate = req.param('verifyDate');
    req.session.accountablesFromMCTIC = req.param('accountablesFromMCTIC');
    req.session.accountablesFromITRAC = req.param('accountablesFromITRAC');
    req.session.isReadyToVerify = true;

    return res.redirect('/pops/list/unverified');
  },
  show: function(req, res) {
    res.locals.styles = AssetsService
      .bootstrapTagsinput.styles
      .concat(AssetsService.jqueryUI.styles);

    res.locals.scripts = AssetsService
      .bootstrapTagsinput.scripts
      .concat(AssetsService.jqueryUI.scripts)
      .concat('js/pages/pops/prepare');

    var endpoint = 'pops/prepare';
    var data = {
      page: '/' + endpoint
    }

    return res.view(endpoint, data);
  }
}
