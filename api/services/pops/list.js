module.exports = {
  list: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/list');

        var endpoint = 'pops/list';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };

        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs!'
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    }

    Pop.getAll(callback);
  },
  unverified: function(req, res) {
    if (req.session.isReadyToVerify &&
      req.session.lastTimeReady === new Date().toDateString()) {

      var callback = function(err, pops) {
        if (!err) {
          res.locals.styles = AssetsService.datatables.styles;
          res.locals.scripts = AssetsService.datatables.scripts
            .concat('js/pages/pops/unverified');

          var endpoint = 'pops/list/unverified';
          var data = {
            page: '/' + endpoint,
            pops: pops
          };

          return res.view(endpoint, data);
        } else {
          var message = 'Erro ao procurar por POPs não verificadas!'
          AlertService.error(req, err, message);
          return res.redirect('/');
        }
      };

      Pop.getAllUnverified(callback);
    } else {
      req.session.isReadyToVerify = false;
      return res.redirect('/pops/prepare_for_verification');
    }
  },
  verified: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/verified');

        var endpoint = 'pops/list/verified';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };

        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs verificadas!'
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllVerified(callback);
  },
  toDelete: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/to_delete');

        var endpoint = 'pops/list/to_delete';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs para excluir!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllToDelete(callback);
  },
  withPendencies: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/with_pendencies');

        var endpoint = 'pops/list/with_pendencies';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs com pendências!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllWithPendencies(callback);
  },
  withoutPendencies: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/without_pendencies');

        var endpoint = 'pops/list/without_pendencies';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs sem pendências!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllWithoutPendencies(callback);
  },
  disposables: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/disposables');

        var endpoint = 'pops/list/disposables';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs descartáveis!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllDisposables(callback);
  },
  updated: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/updated');

        var endpoint = 'pops/list/updated';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs atualizadas!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllUpdated(callback);
  },
  ready: function(req, res) {
    var callback = function(err, pops) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/pops/ready');

        var endpoint = 'pops/list/ready';
        var data = {
          page: '/' + endpoint,
          pops: pops
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por POPs prontas!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Pop.getAllReady(callback);
  }
};
