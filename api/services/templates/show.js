module.exports = {
  show: function(req, res) {
    var id = req.param('id');
    var callback = function(err, template) {
      if (!err) {
        res.locals.scripts = AssetsService.ckeditor.scripts
          .concat('js/pages/templates/show');

        var endpoint = 'templates/show';
        var data = {
          page: '/' + endpoint,
          template: template
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao pesquisar Template no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/templates');
      }
    };

    Template.get(id, callback);
  }
};
