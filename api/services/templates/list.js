module.exports = {
  list: function(req, res) {
    var callback = function(err, templates) {
      if (!err) {
        res.locals.styles = AssetsService.datatables.styles;
        res.locals.scripts = AssetsService.datatables.scripts
          .concat('js/pages/templates/list');

        var endpoint = 'templates/list';
        var data = {
          page: '/' + endpoint,
          templates: templates
        };
        return res.view(endpoint, data);
      } else {
        var message = 'Erro ao procurar por Templates!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    }

    Template.getAll(callback);
  },
  setDefault: function(req, res) {
    var callback = function(err, templates) {
      if (!err) {
        setToDefault(req, res);
      } else {
        var message = 'Erro ao atualizar antigo Template padrão!';
        AlertService.error(req, err, message);
        return res.redirect('/templates/list');
      }
    }

    Template.setAllDefaultToFalse(callback);
  }
};

function setToDefault(req, res) {
  var callback = function(err, template) {
    if (!err) {
      var message = 'Template padrão atualizado com sucesso!';
      AlertService.success(req, message);
    } else {
      var message = 'Erro ao atualizar Template para padrão!';
      AlertService.error(req, err, message);
    }
    return res.redirect('/templates/list');
  }

  Template.setDefault(req.param('id'), callback);
}
