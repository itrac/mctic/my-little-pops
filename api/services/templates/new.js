module.exports = {
  create: function(req, res) {
    var callback = function(err, quantity) {
      if (!err) {
        var isDefault = quantity == 0;
        createTemplate(req, res, isDefault);
      } else {
        var message = 'Erro ao realizar contagem de Templates no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/templates/list');
      }
    }

    Template.getQuantity(callback);
  },
  show: function(req, res) {
    res.locals.scripts = AssetsService.ckeditor.scripts
      .concat('js/pages/templates/show');
    var endpoint = 'templates/new'
    var data = {
      page: '/' + endpoint
    };
    return res.view(endpoint, data);
  }
};

function createTemplate(req, res, isDefault) {
  var template = {
    name: req.param('name'),
    content: req.param('content'),
    creator: req.session.name,
    isDefault: isDefault
  };

  var callback = function(err, template) {
    if (!err) {
      var message = 'Criação de Template bem sucedida!';
      AlertService.success(req, message);
      return res.redirect('/templates/list');
    } else {
      var message = 'Erro ao inserir Template no banco de dados!';
      AlertService.error(req, err, message);
      return res.redirect('/templates/new');
    }
  }

  Template.createOne(template, callback);
}
