module.exports = {
  update: function(req, res) {
    var id = req.param('id');

    var data = {
      name: req.param('name'),
      content: req.param('content')
    };

    var callback = function (err, template) {
      if(!err) {
        var message = 'Atualização do Template bem sucedida!';
        AlertService.success(req, message);
      } else {
        var message = 'Erro ao atualizar Template no banco de dados!';
        AlertService.error(req, err, message);
      }
      return res.redirect('/templates/list');
    }

    Template.updateTemplate(id, data, callback);
  },
  show: function(req, res) {
    var id = req.param('id');
    var callback = function(err, template) {
      if (!err) {
        res.locals.scripts = AssetsService.ckeditor.scripts
        .concat('js/pages/templates/show');
        var endpoint = 'templates/edit';

        if (template) {
          var data = {
            page: '/' + endpoint,
            template: template
          };
          return res.view(endpoint, data);
        } else {
          var data = {
            page: '/' + endpoint
          };
          return res.view(endpoint, data);
        }
      } else {
        req.session.hasTemplate = false;
        var message = 'Erro ao tentar acesar um Template no banco de dados!';
        AlertService.error(req, err, message);
        return res.redirect('/');
      }
    };

    Template.get(id, callback);
  }
};
