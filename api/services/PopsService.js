module.exports = {
  api: require('./pops/api.js'),
  new: require('./pops/new.js'),
  parse: require('./pops/parse.js'),
  show: require('./pops/show.js'),
  edit: require('./pops/edit.js'),
  prepare: require('./pops/prepare.js'),
  list: require('./pops/list.js')
};
