module.exports = {
  readyPops: function(req, res) {
    PopsService.api.readyPops(req, res);
  },
  fivePops: function(req, res) {
    PopsService.api.fivePops(req, res);
  },
  pagedPops: function(req, res) {
    PopsService.api.pagedPops(req, res);
  }
};
