module.exports = {
  new: function(req, res) {
    if (req.method == 'POST') {
      TemplatesService.new.create(req, res);
    } else {
      TemplatesService.new.show(req, res);
    }
  },
  show: function (req, res) {
    TemplatesService.show.show(req, res);
  },
  edit: function(req, res) {
    if (req.method == 'POST') {
      TemplatesService.edit.update(req, res);
    } else {
      TemplatesService.edit.show(req, res);
    }
  },
  list: function(req, res) {
    TemplatesService.list.list(req, res);
  },
  updateDefault: function(req, res) {
    TemplatesService.list.setDefault(req, res);
  }
}
