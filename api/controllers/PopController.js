module.exports = {
  new: function(req, res) {
    if (req.method == 'POST') {
      PopsService.new.createFromForm(req, res);
    } else {
      PopsService.new.show(req, res);
    }
  },
  parse: function(req, res) {
    PopsService.parse.importFromCSV(req, res);
  },
  show: function(req, res) {
    PopsService.show.show(req, res);
  },
  edit: function(req, res) {
    if (req.method == 'POST') {
      PopsService.edit.update(req, res);
    } else {
      PopsService.edit.show(req, res);
    }
  },
  prepareForVerification: function(req, res) {
    if (req.method == 'POST') {
      PopsService.prepare.save(req, res);
    } else {
      PopsService.prepare.show(req, res);
    }
  },
  list: function(req, res) {
    PopsService.list.list(req, res);
  },
  listUnverified: function(req, res) {
    PopsService.list.unverified(req, res);
  },
  verify: function(req, res) {
    var data = {
      id: req.param('id'),
      observations: req.param('observations'),
      actions: req.param('actions'),
      accountablesFromITRAC: req.session.accountablesFromITRAC,
      accountablesFromMCTIC: req.session.accountablesFromMCTIC,
      verifyDate: TimeService.toDatabaseDate(req.session.verifyDate)
    };

    var callback = function(err, updated) {
      if (!err) {
        var message = 'POP atualizada!';
        AlertService.success(req, message);
      } else {
        var message = 'Erro ao atualizar POP!';
        AlertService.error(req, err, message);
      }
      return res.redirect('/pops/list/unverified');
    };

    PopsService.edit.update(req, res, data, callback);
  },
  listVerified: function(req, res) {
    PopsService.list.verified(req, res);
  },
  listToDelete: function(req, res) {
    PopsService.list.toDelete(req, res);
  },
  listWithPendencies: function(req, res) {
    PopsService.list.withPendencies(req, res);
  },
  listWithoutPendencies: function(req, res) {
    PopsService.list.withoutPendencies(req, res);
  },
  listDisposables: function(req, res) {
    PopsService.list.disposables(req, res);
  },
  listUpdated: function(req, res) {
    PopsService.list.updated(req, res);
  },
  listReady: function(req, res) {
    PopsService.list.ready(req, res);
  }
};
