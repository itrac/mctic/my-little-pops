module.exports = {
  login: function(req, res) {
    return res.login({
      email: req.param('email'),
      password: req.param('password'),
      successRedirect: '/',
      invalidRedirect: '/login'
    });
  },
  logout: function(req, res) {
    req.session.me = null;
    return res.redirect('/');
  },
  signup: function(req, res) {
    User.signup({
      name: req.param('name'),
      email: req.param('email'),
      password: req.param('password')
    }, function(err, user) {
      if (err) return res.negotiate(err);
      req.session.me = user.id;
      req.session.name = user.name;
      return res.redirect('/');
    });
  }
};
