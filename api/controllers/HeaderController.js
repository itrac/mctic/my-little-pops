module.exports = {
  new: function(req, res) {
    if (req.method == 'POST') {
      HeadersService.new.create(req, res);
    } else {
      HeadersService.new.show(req, res);
    }
  },
  edit: function(req, res) {
    if (req.method == 'POST') {
      HeadersService.edit.update(req, res);
    } else {
      HeadersService.edit.show(req, res);
    }
  },
  list: function(req, res) {
    HeadersService.list.list(req, res);
  },
  setDefault: function(req, res) {
    HeadersService.list.setDefault(req, res);
  }
}
