module.exports = function login(inputs) {
  inputs = inputs || {};

  var req = this.req;
  var res = this.res;

  User.attemptLogin({
    email: inputs.email
  }, function (err, user) {
    if (err) return res.negotiate(err);
    if (!user) return res.redirect(inputs.invalidRedirect);
    require('bcrypt').compare(inputs.password, user.password, function(err, match) {
      if(match) {
        req.session.me = user.id;
        req.session.name = user.name;
        return res.redirect(inputs.successRedirect);
      } else {
        return res.redirect(inputs.invalidRedirect);
      }
    });
  });
};
