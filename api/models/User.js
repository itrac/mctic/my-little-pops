module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    email: {
      type: 'email',
      required: true
    },
    password: {
      type: 'string',
      required: true
    }
  },
  beforeCreate: function(inputs, next) {
    require('bcrypt').hash(inputs.password, 10, function passwordEncrypted(err, password) {
      if (err) return next(err);
      inputs.password = password;
      next();
    });
  },
  signup: function (inputs, cb) {
    User.create({
      name: inputs.name,
      email: inputs.email,
      password: inputs.password
    })
    .exec(cb);
  },
  attemptLogin: function (inputs, cb) {
    User.findOne({
      email: inputs.email
    })
    .exec(cb);
  }
};
