module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    fields: {
      type: 'json',
      defaultsTo: {}
    },
    author: {
      type: 'string',
      required: true
    },
    isDefault: {
      type: 'boolean',
      defaultsTo: false
    }
  },
  createOne: function(data, cb) {
    var header = {};
    if (data.name) header.name = data.name;
    if (data.fields) header.fields = data.name;
    if (data.author) header.author = data.author;
    if (data.isDefault) header.isDefault = data.isDefault;

    Header.create(header).exec(cb);
  },
  getQuantity: function(cb) {
    Header.count().exec(cb);
  },
  get: function(id, cb) {
    var criteria = {
      id: id
    };

    Header.findOne(criteria).exec(cb);
  },
  getDefault: function(cb) {
    var criteria = {
      isDefault: true
    };
    Header.findOne(criteria).populateAll().exec(cb);
  },
  getAll: function(cb) {
    Header.find().populateAll().exec(cb);
  },
  setAllDefaultToFalse: function(cb) {
    var criteria = {
      isDefault: true
    };

    var data = {
      isDefault: false
    };

    Header.update(criteria, data).exec(cb);
  },
  setDefault: function(id, cb) {
    var criteria = {
      id: id
    };

    var data = {
      isDefault: true
    };

    Header.update(criteria, data).exec(cb);
  },
  updateOne: function(id, data, cb) {
    var criteria = {
      id: id
    };

    Header.update(criteria, data).exec(cb);
  }
};
