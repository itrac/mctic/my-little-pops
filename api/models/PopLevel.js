module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true
    },
    pops: {
      collection: 'Pop',
      via: 'level'
    }
  },
  get: function (inputs, cb) {
    PopLevel.findOrCreate({name: inputs.name}, {name: inputs.name}).exec(cb);
  },
  getAll: function (cb) {
    PopLevel.find().exec(cb);
  }
};
