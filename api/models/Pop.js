module.exports = {
  attributes: {
    number: {
      type: 'string',
      required: true,
      defaultsTo: '0000'
    },
    name: {
      type: 'string',
      required: true
    },
    observations: {
      type: 'string',
      defaultsTo: ''
    },
    actions: {
      type: 'string',
      defaultsTo: ''
    },
    accountablesFromITRAC: {
      type: 'string',
      defaultsTo: ''
    },
    accountablesFromMCTIC: {
      type: 'string',
      defaultsTo: ''
    },
    verifyDate: {
      type: 'date'
    },
    level: {
      model: 'PopLevel'
    },
    procedure: {
      type: 'string',
      defaultsTo: ''
    },
    fields: {
      type: 'json',
      defaultsTo: {}
    },
    state: {
      type: 'string',
      enum: ['Não verificado', 'Verificado', 'Para excluir', 'Com pendências', 'Sem pendências', 'Descartável', 'Atualizado', 'Pronto']
    },
    category: {
      type: 'string',
      enum: [
        'Categoria não definida',
        'Antivírus',
        'Aplicativos de IRPF',
        'Aplicativos de PDF',
        'Certificado Digital',
        'Cliente Java',
        'Compactadores',
        'Comunicadores',
        'Dispositivos Móveis',
        'E-mail',
        'Estação de trabalho',
        'Impressora',
        'Internet e Wi-fi',
        'Login e permissão',
        'Módulos de Segurança Bancário',
        'Navegadores Web',
        'Pacote Office',
        'Pastas de Rede',
        'Player de Aúdio e Vídeo',
        'Software e Aplicativos',
        'Telefonia',
        'Videoconferência',
        'Windows'
      ]
    },
    justification: {
      type: 'string',
      defaultsTo: ''
    },
    referenceSheet: {
      type: 'string',
      defaultsTo: ''
    }
  },
  createOne: function(data, cb) {
    var pop = {};
    if (data.number) pop.number = data.number;
    if (data.name) pop.name = data.name;
    if (data.state) pop.state = data.state;
    if (data.observations) pop.observations = data.observations;
    if (data.actions) pop.actions = data.actions;
    if (data.accountablesFromITRAC) pop.accountablesFromITRAC = data.accountablesFromITRAC;
    if (data.accountablesFromMCTIC) pop.accountablesFromMCTIC = data.accountablesFromMCTIC;
    if (data.verifyDate) pop.verifyDate = data.verifyDate;
    if (data.category) pop.category = data.category;
    if (data.justification) pop.justification = data.justification;
    if (data.referenceSheet) pop.referenceSheet = data.referenceSheet;
    pop.level = data.level ? data.level : {};

    Pop.create(pop)
      .exec(cb);
  },
  createMany: function(pops, cb) {
    Pop.create(pops).exec(cb);
  },
  updateOne: function(id, data, cb) {
    var criteria = {
      id: id
    };

    Pop.update(id, data).exec(cb);
  },
  updateToVerified: function(data, cb) {
    Pop.update({
        id: data.id
      }, {
        state: 'Verificado',
        observations: data.observations,
        actions: data.actions,
        accountablesFromITRAC: data.accountablesFromITRAC,
        accountablesFromMCTIC: data.accountablesFromMCTIC,
        verifyDate: data.verifyDate
      })
      .exec(cb);
  },
  updateToDelete: function(data, cb) {
    Pop.update({
        id: data.id
      }, {
        state: 'Para excluir',
        observations: data.observations,
        actions: data.actions,
        accountablesFromITRAC: data.accountablesFromITRAC,
        accountablesFromMCTIC: data.accountablesFromMCTIC,
        verifyDate: data.verifyDate
      })
      .exec(cb);
  },
  updateProcedure: function(id, procedure, cb) {
    Pop.update({
      id: id
    }, {
      procedure: procedure
    }).exec(cb);
  },
  get: function(id, cb) {
    Pop.findOne({
        id: id
      })
      .populateAll()
      .exec(cb);
  },
  getAll: function(cb) {
    Pop.find().populateAll().exec(cb);
  },
  getAllUnverified: function(cb) {
    var criteria = {
      state: 'Não verificado'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllVerified: function(cb) {
    var criteria = {
      state: 'Verificado'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllToDelete: function(cb) {
    var criteria = {
      state: 'Para excluir'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllWithPendencies: function(cb) {
    var criteria = {
      state: 'Com pendências'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllWithoutPendencies: function(cb) {
    var criteria = {
      state: 'Sem pendências'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllDisposables: function(cb) {
    var criteria = {
      state: 'Descartável'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllUpdated: function(cb) {
    var criteria = {
      state: 'Atualizado'
    };
    Pop.find(criteria).populateAll().exec(cb);
  },
  getAllReady: function(cb) {
    var criteria = {
      state: 'Pronto'
    };
    Pop.find(criteria).populateAll().exec(cb);
  }
};
