module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      defaultsTo: 'POP Template'
    },
    creator: {
      type: 'string',
      required: true,
      defaultsTo: 'criador'
    },
    content: {
      type: 'string',
      required: true,
      defaultsTo: 'Template body'
    },
    isDefault: {
      type: 'boolean',
      defaultsTo: false
    }
  },
  createOne: function(data, cb) {
    var template = {};
    if (data.name) template.name = data.name;
    if (data.creator) template.creator = data.creator;
    if (data.content) template.content = data.content;
    if (data.isDefault) template.isDefault = data.isDefault;

    Template.create(template)
      .exec(cb);
  },
  getQuantity: function(cb) {
    Template.count().exec(cb);
  },
  getDefault: function(cb) {
    var criteria = {
      isDefault: true
    };

    Template.findOne(criteria).populateAll().exec(cb);
  },
  updateTemplate: function(id, data, callback) {
    Template.update(id, data).exec(callback);
  },
  getAll: function(cb) {
    Template.find().populateAll().exec(cb);
  },
  setAllDefaultToFalse: function(cb) {
    var criteria = {
      isDefault: true
    };

    var data = {
      isDefault: false
    };

    Template.update(criteria, data).exec(cb);
  },
  setDefault: function(id, cb) {
    var criteria = {
      id: id
    };

    var data = {
      isDefault: true
    };

    Template.update(criteria, data).exec(cb);
  },
  get: function(id, cb) {
    var criteria = {
      id: id
    };

    Template.findOne(criteria).populateAll().exec(cb);
  }
};
